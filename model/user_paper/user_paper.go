package user_paper

import (

)

type UserPaper struct {
  ID int `gorm:"column:id primaryKey" json:"id"`
  UserID int `gorm:"column:user_id" json:"user_id"`
  ExamID int `gorm:"column:exam_id" json:"exam_id"`
  PaperID int `gorm:"column:paper_id" json:"paper_id"`
  Score string `gorm:"column:score" json:"score"`
  UserOption string `gorm:"column:user_option" json:"-"`
  UserOptionMap []map[string]map[string]string `gorm:"-" json:"user_option"`
  CreatedAt string `gorm:"column:created_at" json:"created_at"`
}

func (r UserPaper) TableName() string {
  return "user_paper"
}

func Get() UserPaper {
  return UserPaper{}
}