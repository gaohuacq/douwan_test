package question

import (

)

type Question struct {
  ID int `gorm:"column:id primaryKey" json:"id"`
  Type int `gorm:"column:type" json:"type"`
  Title string `gorm:"column:title" json:"title"`
}

func (r Question) TableName() string {
  return "question"
}

func Get() Question {
  return Question{}
}