package question_option

import (

)

type QuestionOption struct {
  ID int `gorm:"column:id primaryKey" json:"id"`
  QuestionID int `gorm:"column:question_id" json:"question_id"`
  Title string `gorm:"column:title" json:"title"`
  IsRight int `gorm:"column:is_right" json:"is_right"`
  Seq int `gorm:"column:seq" json:"seq"`
}

func (r QuestionOption) TableName() string {
  return "question_option"
}

func Get() QuestionOption {
  return QuestionOption{}
}