package user

import(
  
)

type User struct {
  ID int `gorm:"primaryKey column:id" json:"id"`
  Username string `gorm:"column:username" json:"username"`
  Password string `gorm:"column:password" json:"password"`
  Grade string `grom:"column:grade" json:"grade"`
  CreatedAt string `gorm:"column:created_at" json:"created_at"`
}

func (r User) TableName() string {
  return "user"
}

func Get() User {
  return User{}
}