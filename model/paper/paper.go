package paper

import (

)

type Paper struct {
  ID int `gorm:"column:id primaryKey" json:"id"`
  Title string `gorm:"column:title" json:"title"`
  Question string `gorm:"column:question" json:"-"`
  QuestionMap []map[string]string `gorm:"-" json:"question"`
}

func (r Paper) TableName() string {
  return "paper"
}

func Get() Paper {
  return Paper{}
}