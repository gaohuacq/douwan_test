package exam

import (

)

type Exam struct {
  ID int `gorm:"column:id primaryKey" json:"id"`
  Title string `gorm:"column:title" json:"title"`
  PaperA int `gorm:"column:paper_a" json:"paper_a"`
  PaperB int `gorm:"column:paper_b" json:"paper_b"`
}

func (r Exam) TableName() string {
  return "exam"
}

func Get() Exam {
  return Exam{}
}