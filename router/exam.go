package router

import (
  "github.com/gin-gonic/gin"

  "douwan_test/config"
  "douwan_test/model/exam"
  "douwan_test/model/paper"
)


type Exam struct {
  handler config.Handler
  return_data ReturnData
}

func ExamHandler(h config.Handler) Exam {
  return Exam{handler: h}
}

func (r Exam) Bind() {
  g := r.handler.Router.Group("/exam")
  {
    g.GET("/list", r.ExamList)
    g.GET("/fetch_paper", r.PaperInfo)
    g.POST("/save_user_paper", r.SaveUserPaper)
    g.GET("/user_paper", r.UserPaper)
    g.GET("/user_take_list", r.UserTakeList)
    g.GET("/user_take_info", r.UserTakeInfo)
    g.GET("/take_list", r.TakeList)
  }
  r.handler.DB.AutoMigrate(&exam.Exam{}, &paper.Paper{})
}

func (r Exam) ExamList(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]string{
    map[string]string{
      "id": "1",
      "title": "测试1",
      "paper_a": "1",
      "paper_b": "2",
    },
    map[string]string{
      "id": "2",
      "title": "测试2",
      "paper_a": "3",
      "paper_b": "4",
    },
  }
}

func (r Exam) PaperInfo(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]interface{}{
    map[string]interface{}{
      "id": "1",
      "title": "题目1",
      "score": "2",
      "type": "1",
      "options": []map[string]string{
        map[string]string{
          "id": "1",
          "title": "选项A",
          "is_right": "0",
        },
        map[string]string{
          "id": "2",
          "title": "选项B",
          "is_right": "1",
        },
      },
    },map[string]interface{}{
      "id": "2",
      "title": "题目2",
      "score": "3",
      "type": "2",
      "options": []map[string]string{
        map[string]string{
          "id": "3",
          "title": "选项A",
          "is_right": "1",
        },
        map[string]string{
          "id": "4",
          "title": "选项B",
          "is_right": "1",
        },
      },
    },
  }
}

func (r Exam) SaveUserPaper(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = "success"
}

func (r Exam) UserPaper(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = "user_paper"
}

func (r Exam) UserTakeList(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]string{
    map[string]string{
      "exam_id": "1",
      "exam_title": "考试1",
      "paper_id": "1",
      "paper_title": "测试试卷",
      "created_at": "1652002722",
    },
    map[string]string{
      "exam_id": "2",
      "exam_title": "考试2",
      "paper_id": "2",
      "paper_title": "测试试卷2",
      "created_at": "1652002723",
    },
  } 
}

func (r Exam) UserTakeInfo(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]interface{}{
    map[string]interface{}{
      "question_id": "1",
      "question_title": "题目1",
      "question_score": "2",
      "question_type": "1",
      "user_score": "2",
      "user_options": []int{1},
      "options": []map[string]string{
        map[string]string{
          "id": "1",
          "title": "选项1",
          "is_right": "0",
        },
        map[string]string{
          "id": "2",
          "title": "选项2",
          "is_right": "1",
        },
      },
    },
    map[string]interface{}{
      "question_id": "2",
      "question_title": "题目2",
      "question_score": "4",
      "question_type": "1",
      "user_score": "0",
      "user_options": []int{4,5},
      "options": []map[string]string{
        map[string]string{
          "id": "4",
          "title": "选项4",
          "is_right": "1",
        },
        map[string]string{
          "id": "5",
          "title": "选项5",
          "is_right": "1",
        },
      },
    },
  }
}

func (r Exam) TakeList(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]string{
    map[string]string{
      "id": "1",
      "exam_id": "1",
      "exam_title": "考试1",
      "paper_id": "1",
      "paper_title": "试卷1",
      "user_id": "1",
      "username": "test",
      "score": "80",
      "created_at": "1652002722",
    },
    map[string]string{
      "id": "2",
      "exam_id": "2",
      "exam_title": "考试2",
      "paper_id": "3",
      "paper_title": "试卷3",
      "user_id": "1",
      "username": "test",
      "score": "90",
      "created_at": "1652002712",
    },
  }
}