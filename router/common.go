package router

import (
  "github.com/gin-gonic/gin"
)

var (
  client, token, version string
  uid int
)

type ReturnData struct {
  status int
  info string
  data interface{}
}

func return_json(c *gin.Context, data *ReturnData) {
  c.JSON(200, gin.H{
    "status": data.status,
    "info": data.info,
    "data": data.data,
  })
}