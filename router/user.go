package router

import (
  "github.com/gin-gonic/gin"

  "douwan_test/config"
  "douwan_test/model/user"
)

type User struct {
  handler config.Handler
  return_data ReturnData
}

func UserHandler(h config.Handler) User {
  return User{handler: h}
}

func (r User) Bind() {
  g := r.handler.Router.Group("/user")
  {
    g.POST("/login", r.Login)
    g.POST("/register", r.Register)
    g.GET("/info", r.UserInfo)
    g.GET("/list", r.UserList)
  }
  r.handler.DB.AutoMigrate(&user.User{})
}

func (r User) Login(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = map[string]string{
    "uid": "1",
    "token": "alk34h123aebee423434sdf",
    "username": "test",
  }
}

func (r User) Register(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = map[string]string{
    "uid": "1",
    "token": "alk34h123aebee423434sdf",
    "username": "test",
  }
}

func (r User) UserInfo(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = map[string]string{
    "uid": "1",
    "username": "test",
    "grade": "6",
    "created_at": "1652002722",
  }
}

func (r User) UserList(c *gin.Context) {
  defer return_json(c, &r.return_data)

  r.return_data.status = 1
  r.return_data.data = []map[string]string{
    map[string]string{
      "id": "1",
      "username": "test",
      "grade": "6",
      "created_at": "1652002722",
    },
    map[string]string{
      "id": "2",
      "username": "test2",
      "grade": "5",
      "created_at": "1652000722",
    },
  }
}