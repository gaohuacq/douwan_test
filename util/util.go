package util

import(
  "github.com/gin-gonic/gin"
  "github.com/nfnt/resize"
  "html/template"
  "github.com/go-redis/redis/v8"
  "io"
  "io/ioutil"
  "os"
  "time"
  "regexp"
  "strings"
  "crypto/md5"
  "encoding/hex"
  "math"
  "math/rand"
  "strconv"
  "net/url"
  // "net/http"
  "encoding/json"
  "unicode/utf8"
  "context"
  "reflect"

"fmt"
"log"

  "image"
  "image/jpeg"
  "image/png"
  "image/gif"
  "github.com/shamsher31/goimgtype"

  "douwan_test/config"
)

var (
  ctx = context.Background()
  redis_prefix = "yysub_"
)

func blank() {
  fmt.Print("")
  log.Println("")
  reflect.TypeOf("123")
}

func LogPrintln(values ...interface{}){
  log.Println(values)
}

func SaveUploadFile(c *gin.Context, filename string) string {
  filepath := "./res/ftp/poster/"+time.Now().Format("2006")+"/"+time.Now().Format("0102")+"/"
  file, _ := c.FormFile(filename)
  if file==nil {
    return ""
  }

  reg := regexp.MustCompile(`(?i)\.([a-z]{2,}$)`)
  param := reg.FindStringSubmatch(file.Filename)
  if param == nil {
    return ""
  }
  ext := strings.ToLower(param[1])

  CheckPathExist(filepath)

  new_filename := filepath+Md5String(file.Filename+"_"+RandToString())+"."+ext
  c.SaveUploadedFile(file, new_filename)

  return new_filename
}

/*检查文件夹是否存在，不存在则创建*/
func CheckPathExist(path string) bool {
  exist := IsExist(path)
  if exist == false {
    os.MkdirAll(path, os.ModePerm)
  }
  return exist
}

/*判断文件或目录是否存在*/
func IsExist(path string) bool {
  _, err := os.Stat(path)
  if err == nil {
      return true
  }
  if os.IsNotExist(err) {
      return false
  }
  return false
}

/*获取文件夹下的所有文件，包括子文件夹*/
func GetAllFiles(dirPth string) (files []string, err error) {
  var dirs []string
  dir, err := ioutil.ReadDir(dirPth)
  if err != nil {
      return nil, err
  }

  PthSep := string(os.PathSeparator)
  //suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

  for _, fi := range dir {
    if fi.IsDir() { // 目录, 递归遍历
      dirs = append(dirs, dirPth+PthSep+fi.Name())
      GetAllFiles(dirPth + PthSep + fi.Name())
    } else {
      // 过滤指定格式
      if ok := strings.HasSuffix(fi.Name(), ".go");ok == false {
        files = append(files, dirPth+PthSep+fi.Name())
      }
    }
  }

  // 读取子目录下文件
  for _, table := range dirs {
    temp, _ := GetAllFiles(table)
    for _, temp1 := range temp {
      files = append(files, temp1)
    }
  }

  return files, nil
}

func Md5String(str string) string  {
    h := md5.New()
    h.Write([]byte(str))
    return hex.EncodeToString(h.Sum(nil))
}

func RandToString() string {
  return Int64ToString(rand.New(rand.NewSource(time.Now().UnixNano())).Int63())
}

func Random(min, max int) int {
  return rand.Intn(max - min) + min
}

func GetClientIp(c *gin.Context) string {
  reqIP := c.ClientIP()
  if reqIP == "::1" {
      reqIP = "127.0.0.1"
  }

  return reqIP
  /*addrs, err := net.InterfaceAddrs()

  if err != nil {
    return ""
  }

  for _, address := range addrs {
    // 检查ip地址判断是否回环地址
    if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
      if ipnet.IP.To4() != nil {
        return ipnet.IP.String()
      }

    }
  }

  return ""*/

}

func JoinSqlWhere(condition map[string]interface{}) (string, map[string]interface{}) {
  key := []string{}
  value := map[string]interface{}{}
  for k,v := range condition {
    if k == "keyword" {
      key = append(key, "(cnname like @"+k+" || enname like @"+k+")")
      value[k] = "%"+v.(string)+"%"
    } else {
      key = append(key, k+" = @"+k)
      value[k] = v
    }
  }

  return strings.Join(key," && "),value
}

func StringSlice(str string,from int, offset int) string {
  if str == "" {
    return ""
  }
  length := utf8.RuneCountInString(str)
  if length < offset {
    offset = length
  }
  if offset < 1 {
    offset = length + offset
  }
  return string([]rune(str)[from :offset])
}

func PosterFullUrl(str string) string {
  if str == "" {
    return ""
  } else {
    return config.C.Url.Poster+str
  }
}

/*
$_seasons = array(
      array('id'=>0,'name'=>'前传'),
      array('id'=>101,'name'=>'单剧'),
      array('id'=>102,'name'=>'mini剧'),
      array('id'=>103,'name'=>'周边资源')
    );
 */

func FormatSeasonEpisode(season string, episode string, lang string) string {
  if StringToInt(season) < 1 || StringToInt(season) > 100 {
    seasonArray := map[string]string{"0":"前传", "101":"单剧", "102":"mini剧", "103":"周边资源"}
    season = seasonArray[season]
    if lang == "cn" {
      return season+"第"+episode+"季"
    } else {
      if len(episode)<2 {
        episode = "0"+episode
      }
      return season+"E"+episode
    }
  } else {
    if lang == "cn" {
      return "第"+season+"季第"+episode+"集"
    } else {
      if len(season)<2 {
        season = "0"+season
      }
      if len(episode)<2 {
        episode = "0"+episode
      }
      return "S"+season+"E"+episode
    }
  }
}

func Ip2Long(ipstr string) (ip int64) {
  r := `^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})`
  reg, err := regexp.Compile(r)
  if err != nil {
      return
  }
  ips := reg.FindStringSubmatch(ipstr)
  if ips == nil {
      return
  } 

  ip1, _ := strconv.Atoi(ips[1])
  ip2, _ := strconv.Atoi(ips[2])
  ip3, _ := strconv.Atoi(ips[3])
  ip4, _ := strconv.Atoi(ips[4])

  if ip1>255 || ip2>255 || ip3>255 || ip4 > 255 {
      return
  }

  ip += int64(ip1 * 0x1000000)
  ip += int64(ip2 * 0x10000)
  ip += int64(ip3 * 0x100)
  ip += int64(ip4)

  return
}
func Long2Ip(ip int64) string {
  return fmt.Sprintf("%d.%d.%d.%d", ip>>24, ip<<8>>24, ip<<16>>24, ip<<24>>24)
}

func TimeNow() int64 {
  var cstZone = time.FixedZone("GMT", 8*3600)
  return time.Now().In(cstZone).Unix()
}

func TimeFormat(format string, unixtime int64) string {
  var cstZone = time.FixedZone("GMT", 8*3600)
  if format != "" {
    format = strings.ReplaceAll(format, "Y", "2006")
    format = strings.ReplaceAll(format, "m", "01")
    format = strings.ReplaceAll(format, "d", "02")
    format = strings.ReplaceAll(format, "H", "15")
    format = strings.ReplaceAll(format, "i", "04")
    format = strings.ReplaceAll(format, "s", "05")
  } else {
    format = "2006-01-02 15:04:05"
  }

  if unixtime == 0 {
    return time.Now().In(cstZone).Format(format)
  }
  return time.Unix(unixtime, 0).In(cstZone).Format(format)
}

func Weekday(unixtime int64) string {
  var cstZone = time.FixedZone("GMT", 8*3600)
  week_array := []string{"周日","周一","周二","周三","周四","周五","周六"}
  return week_array[time.Unix(unixtime, 0).In(cstZone).Weekday()]
}

func ParseDate(date string) int64 {
  loc, _ := time.LoadLocation("Asia/Shanghai")
  tt, _ := time.ParseInLocation("2006-01-02 15:04:05", date+" 00:00:00", loc)
  return tt.Unix()
}

func Pagetion(cur_page int, limit int, count int64, uri url.Values) string {
  total := int(math.Ceil(float64(count)/float64(limit)))
  html := ""
  r := 5
  for i:=cur_page-r;i<=cur_page+r; i++ {
    if i < 1 {
      continue
    } else if i > total {
      break
    }else if i== cur_page {
      html += "<a class=\"cur\">"+IntToString(i)+"</a>"  
    } else {
      uri.Set("p",IntToString(i))
      html += "<a href=\"?"+uri.Encode()+"\">"+IntToString(i)+"</a>"
    }
  }
  if cur_page > 1 {
    uri.Set("p",IntToString(cur_page-1))
    html = "<a href=\"?"+uri.Encode()+"\">上一页</a>"+html
  }
  if cur_page < total {
    uri.Set("p",IntToString(cur_page+1))
    html += "<a href=\"?"+uri.Encode()+"\">下一页</a>"
  }
  if cur_page-r > 1 {
    uri.Set("p", "1")
    html = "<a href=\"?"+uri.Encode()+"\">首页</a>"+html
  }
  if total-r > cur_page {
    uri.Set("p", IntToString(total))
    html += "<a href=\"?"+uri.Encode()+"\">末页</a>"
  }
  if html == "" {
    html = "<a class=\"cur\">1</a>"  
  }

  return html
}

func IntToString(s int) string {
  return strconv.Itoa(s)
}

func Int32ToString(s int32) string {
  return strconv.FormatInt(int64(s),10)
}

func Int64ToString(s int64) string {
  return strconv.FormatInt(s,10)
}

func StringToInt(s string) int {
  result,_ := strconv.Atoi(s)
  return result
}

func StringToInt64(s string) int64 {
  result, _ := strconv.ParseInt(s, 10, 64)
  return result
}

func StringToFloat64(s string) float64 {
  result, _ := strconv.ParseFloat(s, 64)
  return result
}

func Float64ToString(s float64) string {
  return strconv.FormatFloat(s, 'f', -1, 64)
}

func IntSliceToString(s []int) []string {
  d := []string{}
  for _,v := range s {
    d = append(d, IntToString(v))
  }
  return d
}

func StringSliceToInt(s []string) []int {
  d := []int{}
  for _,v := range s {
    d = append(d, StringToInt(v))
  }
  return d
}

func InSlice(needle interface{}, hystack interface{}) bool {
  switch key := needle.(type) {
    case string:
      for _, item := range hystack.([]string) {
        if key == item {
          return true
        }
      }
    case int:
      for _, item := range hystack.([]int) {
        if key == item {
          return true
        }
      }
    case int64:
      for _, item := range hystack.([]int64) {
        if key == item {
          return true
        }
      }
    default:
      return false
  }
  return false
}

func UniqueIntSlice(slice []int) []int {
  keys := make(map[int]bool)
  list := []int{} 
  for _, entry := range slice {
    if _, value := keys[entry]; !value {
      keys[entry] = true
      list = append(list, entry)
    }
  }    
  return list
}

func UniqueStringSlice(slice []string) []string {
  keys := make(map[string]bool)
  list := []string{} 
  for _, entry := range slice {
    if _, value := keys[entry]; !value {
      keys[entry] = true
      list = append(list, entry)
    }
  }    
  return list
}

func JsonEncode(v interface{}) string {
  data,_ := json.Marshal(v)
  return string(data)
}

func JsonDecode(data []byte, v interface{}) {
  json.Unmarshal(data, v)
}

func Nl2br(str string) string {
  return strings.Replace(str, "\n", "<br />", -1)
}

func IsEmail(email string) bool {
  if regexp.MustCompile(`(?i)@([a-z0-9]+\.){1,}([a-z]{2,})$`).FindStringSubmatch(email) == nil {
    return false
  } else {
    return true
  }
}

func ResizeImage(imagePath string, sizeConfig interface{}) (bool, error) {
  file, _ := os.Open(imagePath)
  defer file.Close()

  imgType, _ := imgtype.Get(imagePath)

  if imgType != "image/jpeg" && imgType != "image/png" && imgType != "image/gif" {
    return false,nil
  }

  img, _, err := image.Decode(file)
  if err != nil {
      return false,err
  }

  b := img.Bounds()
  src_width := b.Max.X
  src_height := b.Max.Y

  var resize_width,resize_height int
  var c_width,c_height,scale float64
  var savePath string

  extReg := regexp.MustCompile(`(?i)(.+)\.([a-z]{2,}$)`)
  regParams := extReg.FindStringSubmatch(imagePath)

  for format,size := range sizeConfig.(map[string]interface{}) {
    sizeArray := size.(map[string]interface{})

    c_width = float64(sizeArray["width"].(int))
    c_height = float64(sizeArray["height"].(int))

    if regParams != nil {
      savePath = regParams[1]+"_"+format+"."+regParams[2]
    } else {
      savePath = imagePath+"_"+format+"."+StringSlice(imgType,6,-1)
    }

    scale = math.Min(c_width/float64(src_width), c_height/float64(src_height))
    if scale >= 1 {
      CopyFile(imagePath, savePath)
    } else {
      resize_width = int(math.Ceil(float64(src_width)*scale))
      resize_height = int(math.Ceil(float64(src_height)*scale))

      newimage := resize.Resize(uint(resize_width), uint(resize_height), img, resize.Lanczos3)
      imgfile, _ := os.Create(savePath)
      defer imgfile.Close()

      if imgType == "image/png" {
        err = png.Encode(imgfile, newimage)  
      } else if imgType == "image/png"  {
        err = jpeg.Encode(imgfile, newimage, &jpeg.Options{100})
      } else {
        err = gif.Encode(imgfile, newimage, nil) 
      }
      if err != nil {
          return false,err
      }
    }
  }

  return true,nil
}

func StringRegxReplace(s, regx, to string) string {
  extReg := regexp.MustCompile(regx)
  return extReg.ReplaceAllString(s, to)
}

/*
'b' => array('w'=>280,'h'=>390),
'm' => array('w'=>122,'h'=>162),
's' => array('w'=>60,'h'=>76)
 */

func GetImageThumb(filePath string, thumb string, channel string) string {
  var thumb_size map[string]map[string]string

  if channel == "resource" {
    thumb_size = map[string]map[string]string{
      "b": {"w":"280", "h":"390"},
      "m": {"w":"122", "h":"162"},
      "s": {"w":"60", "h":"76"},
    }
  } else if channel == "article" {
    thumb_size = map[string]map[string]string{
      "b": {"w":"900", "h":"500"},
      "m": {"w":"216", "h":"120"},
      "s": {"w":"100", "h":"56"},
    }
  }

  extReg := regexp.MustCompile(`(?i)^(http[s]?\:)?\/\/`)
  if regParams := extReg.FindStringSubmatch(filePath);regParams == nil {
    filePath = config.C.Url.Ftp+filePath
  }

  extReg = regexp.MustCompile(`(@[0-9,]+)(@[0-9,]+)?(\.webp)?`)
  if regParams := extReg.FindStringSubmatch(filePath);regParams != nil {
    return extReg.ReplaceAllString(filePath, "@"+thumb_size[thumb]["w"]+","+thumb_size[thumb]["h"]+"$2.webp")
  } else {
    return regexp.MustCompile(`\.(jpg|jpeg|png)(\.webp)?$`).ReplaceAllString(filePath, ".$1@"+thumb_size[thumb]["w"]+","+thumb_size[thumb]["h"]+".webp")
  }

  return filePath
}

func GetAllThumb(filepath string, channel string) map[string]string {
  data := map[string]string{
    "b": GetImageThumb(filepath, "b", channel),
    "m": GetImageThumb(filepath, "m", channel),
    "s": GetImageThumb(filepath, "s", channel), 
  }
  return data
}

func ReadFile(filename string) string {
  f, err := os.Open(filename)
  if err != nil {
      fmt.Println("read file fail", err)
      return ""
  }
  defer f.Close()

  fd, err := ioutil.ReadAll(f)
  if err != nil {
      fmt.Println("read to fd fail", err)
      return ""
  }

  return string(fd)
}

func CopyFile(srcName, dstName string) (written int64, err error) {
  src, err := os.Open(srcName)
  if err != nil {
      return
  }
  defer src.Close()

  dst, err := os.OpenFile(dstName, os.O_WRONLY|os.O_CREATE, 0644)
  if err != nil {
      return
  }
  defer dst.Close()

  return io.Copy(dst, src)
}

func RedisKey(key string) string {
  return redis_prefix+key
}

func RedisExists(rdb *redis.Client, key string) (int64, error) {
  return rdb.Exists(ctx, key).Result()
}

func RedisExpire(rdb *redis.Client, key string, expiration time.Duration) *redis.BoolCmd {
  return rdb.Expire(ctx, key, expiration)
}

func RedisGet(rdb *redis.Client, key string) *redis.StringCmd {
  return rdb.Get(ctx, key)
}

func RedisSet(rdb *redis.Client, key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
  return rdb.Set(ctx, key, value, expiration)
}

func RedisDel(rdb *redis.Client, key string) *redis.IntCmd {
  return rdb.Del(ctx, key)
}

func RedisIncr(rdb *redis.Client, key string) *redis.IntCmd {
  return rdb.Incr(ctx, key)
}

func RedisHGet(rdb *redis.Client, key, field string) *redis.StringCmd {
  return rdb.HGet(ctx, key, field)
}

func RedisHSet(rdb *redis.Client, key string, values ...interface{}) *redis.IntCmd {
  return rdb.HSet(ctx, key, values)
}

func RedisSAdd(rdb *redis.Client, key string, members ...interface{}) *redis.IntCmd {
  return rdb.SAdd(ctx, key, members)
}

func RedisSMember(rdb *redis.Client, key string) *redis.StringSliceCmd {
  return rdb.SMembers(ctx, key)
}

func RedisSRem(rdb *redis.Client, key string, members ...interface{}) *redis.IntCmd {
  return rdb.SRem(ctx, key, members)
}

func RedisZAdd(rdb *redis.Client, key string, members ...*redis.Z) *redis.IntCmd {
  return rdb.ZAdd(ctx, key, members...)
}

func RedisZRank(rdb *redis.Client, key string, member string) *redis.IntCmd {
  return rdb.ZRank(ctx, key, member)
}

func RedisZScore(rdb *redis.Client, key string, member string) *redis.FloatCmd {
  return rdb.ZScore(ctx, key, member)
}

func RedisZRevRange(rdb *redis.Client, key string, start, stop int64) *redis.StringSliceCmd {
  return rdb.ZRevRange(ctx, key, start, stop)
}

func RedisZRemRangeByRank(rdb *redis.Client, key string, start, stop int64) *redis.IntCmd {
  return rdb.ZRemRangeByRank(ctx, key, start, stop)
}

func RedisZRem(rdb *redis.Client, key string, member string) *redis.IntCmd {
  return rdb.ZRem(ctx, key, member)
}

func RedisZCard(rdb *redis.Client, key string) int64 {
  return rdb.ZCard(ctx, key).Val()
}

func SaveCacheKey(rdb *redis.Client, cache_key string, member interface{}, expire time.Duration) {
  RedisSAdd(rdb, cache_key, member)
  RedisExpire(rdb, cache_key, expire)
}

func FlushCacheKey(rdb *redis.Client, cache_key string) {
  key_array, _ := RedisSMember(rdb, cache_key).Result()
  for _, key := range key_array {
    RedisDel(rdb, key)
  }
  RedisDel(rdb, cache_key)
}

func UpdateYYetsCache(rdb *redis.Client, action string, args ...string) {
  var cache_keys = []string{}
  switch action {
    case "resource":
      cache_keys = append(cache_keys, "resource:"+args[0])
    case "user":
      cache_keys = append(cache_keys, "user:"+args[0])
  }

  for _, key := range cache_keys {
    RedisDel(rdb, key)
  }

  /*accesskey := "ABCyysub2021+$"
  response, err := http.PostForm(
    config.C.Url.Www+"cache/update",
    url.Values{
      "key": {accesskey},
      "action": {action},
      "args[]": args,
    },
  )
  if err != nil {
      fmt.Printf("%v", err)
  }
  defer response.Body.Close()
  body, err := ioutil.ReadAll(response.Body)
  fmt.Println(string(body))*/
}

func LazyUpdateView(rdb *redis.Client, table_name string, field string, id int, step int) (int,bool) {
  expire := int64(60)
  ctx := context.Background()
  now_time := time.Now().Unix()
  cache_key := "yysub_util/LazyUpdateView/"+table_name+"/"+field+"/"+IntToString(id)
  _step, _ := rdb.Do(ctx, "hget", cache_key, "step").Int()
  _expire, _ := rdb.Do(ctx, "hget", cache_key, "expire").Int64()

  if _step == 0 && _expire == 0 {
    rdb.Do(ctx, "hmset", cache_key, "step", step, "expire", now_time+expire)
    return 0,false
  } else if _expire < now_time{
    rdb.Do(ctx, "del", cache_key)
    return (_step+step),true
  } else {
    rdb.Do(ctx, "hincrby", cache_key, "step", int64(step))
    return 0,false
  }
}

func TemplateHTML(str string) template.HTML {
  return template.HTML(str)
}

func TemplateJS(str string) template.JS {
  return template.JS(str)
}

func TemplateJoin(str ...string) string {
  return strings.Join(str,"")
}