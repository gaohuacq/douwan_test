package main

import(
  "github.com/gin-gonic/gin"
  "log"

  "douwan_test/config"
  "douwan_test/router"
  // "douwan_test/util"
)

var handler config.Handler

func main() {
  config.Init()
  handler.DB = config.ConnDB()
  handler.Redis = config.H.Redis
  // handler.Session = session.InitSession()

  if config.C.RunMode == "debug" {
    gin.SetMode(gin.DebugMode)
    handler.Router = gin.Default()
  } else {
    gin.SetMode(gin.ReleaseMode)
    handler.Router = gin.New()
    handler.Router.Use(gin.Recovery())
  }

  /*设置路由*/
  type RouterList interface {
    Bind()
  }

  routerList := []RouterList{
    router.UserHandler(handler),
    router.ExamHandler(handler),
  }

  for _,r := range routerList {
    r.Bind()
  }

  log.Println("Server start")
  handler.Router.Run("127.0.0.1:8088")
}