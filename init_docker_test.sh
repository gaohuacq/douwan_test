#!/usr/bin/env bash
export COMPOSE_PROJECT_NAME="web"
docker-compose -f docker/docker-compose-test.yml down
docker-compose -f docker/docker-compose-test.yml up -d