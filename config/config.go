package config

import (
  "github.com/gin-gonic/gin"
  "github.com/spf13/viper"
  "github.com/fsnotify/fsnotify"
  "log"
  "gorm.io/gorm"
  "gorm.io/driver/mysql"
  // "github.com/gorilla/sessions"
  "context"
  "github.com/go-redis/redis/v8"
  "time"
  "fmt"
  "os"
  // "os/exec"
  // "path/filepath"
  // "regexp"
)

var (
  C Config
  H Handler
  config *viper.Viper
  config_extend *viper.Viper
  err error
  run_mode string
)

type Config struct {
  RunMode string `mapstructure:"runmode"`
  WebName string `mapstructure:"webname"`
  YoutubePath string `mapstructure:"youtube_path"`
  WorkDir string
  Mysql struct {
    Dsn string
  }
  Resource struct {
    Channel map[string]string
    Category []string
    Season map[string]string
  }
  Cookie struct {
    Prefix string
  }
  Redis struct {
    Address string
    Db int
  }
  // Session struct {
  //   Name string
  //   MaxAge int `mapstructure:"max_age"`
  // }
  /*Channel map[string]string `json:"channel"`
  Profession map[string]string `json:"profession"`
  Url struct {
    Js string
    Css string
    Image string
    Ftp string
    Poster string
    Admin string
    Www string
  }
  Weixin struct {
    Appid string
    Appsecret string
  }
  ImageThumb struct {
    Big struct {
      Width int
      Height int
    } `mapstructure:"b"`
    Medium struct {
      Width int
      Height int
    } `mapstructure:"m"`
    Small struct {
      Width int
      Height int
    } `mapstructure:"s"`
  } `mapstructure:"image_thumb"`
  Api struct {
    Accesskey string
  }
  Version struct {
    Android struct {
      MiniVersion int `mapstructure:"mini_version""`
      LastestVersion int `mapstructure:"lastest_version"`
      DownloadUrl string `mapstructure:"download_url"`
      Message string `mapstructure:"message"`
    }
    IOS struct {
      DownloadUrl string `mapstructure:"download_url"`
    }
  }*/
}

type Handler struct {
  Router *gin.Engine
  DB *gorm.DB
  // Session *sessions.CookieStore
  Redis *redis.Client
}

func Init() error {
  run_mode = os.Getenv("RUNMODE")
  C.WorkDir = "."
  /*if run_mode != "release" {
    curFilename := os.Args[0]
    Path, _ := exec.LookPath(curFilename)
    binaryPath, _ := filepath.Abs(Path)
    C.WorkDir = filepath.Dir(binaryPath)
    extReg := regexp.MustCompile(`\/tmp\/?$`)
    C.WorkDir = extReg.ReplaceAllString(C.WorkDir,"")
  }else{
    C.WorkDir = "."
  }*/

  if err = InitConfig(); err != nil {
    fmt.Fprintf(os.Stderr, "init config error: %s\n", err.Error())
    return err
  }
  watchConfig(config, "config")

  // if err = InitConfigExtend(); err != nil {
  //   fmt.Fprintf(os.Stderr, "init config error: %s\n", err.Error())
  //   return err
  // }
  // watchConfig(config_extend, "config_extend")

  InitRedis()
  return nil
}

func InitConfig() error  {
  config = viper.New()
  if run_mode != "release" {
    config.SetConfigFile(C.WorkDir+"/config/config_debug.yaml")
  } else {
    config.SetConfigFile(C.WorkDir+"/config/config.yaml")
  }
  // config.AutomaticEnv()
  // config.SetEnvPrefix("yyets")

  if err := config.ReadInConfig();err != nil {
    panic(fmt.Errorf("Fatal error config file: %s \n", err))
  }

  // 将配置参数反序列到变量C
  if err := config.Unmarshal(&C); err != nil {
    return err
  }

  C.RunMode = run_mode

  return nil
}

func InitConfigExtend() error {
  config_extend = viper.New()
  config_extend.SetConfigFile(C.WorkDir+"/config/config_extend.json")
  if err := config_extend.ReadInConfig();err != nil {
    panic(fmt.Errorf("Fatal error config file: %s \n", err))
  }

  return nil
}

func InitRedis() error {
  H.Redis = redis.NewClient(&redis.Options{
    Addr:     C.Redis.Address,
    Password: "",
    DB:       C.Redis.Db,
  })
  ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
  defer cancel()

  _, err := H.Redis.Ping(ctx).Result()
  return err
}

func StaticGet(s string) interface{} {
  return config.Get(s)
}

func Get(s string) interface{} {
  return config_extend.Get(s)
}

func GetConfig(s string) string {
  return config_extend.GetString(s)
}

func GetString(s string) string {
  return config_extend.GetString(s)
}

func GetInt(s string) int {
  return config_extend.Get(s).(int)
}

func GetFloat64(s string) float64 {
  return config_extend.Get(s).(float64)
}

func GetStringMap(s string) []string {
  return config_extend.GetStringSlice(s)
}

func GetMap(s string) map[string]interface{} {
  return config_extend.Get(s).(map[string]interface{})
}

func GetResourceChannel() map[string]interface{} {
  return GetMap("resource.channel")
}

func GetResourceCategory() map[string]interface{} {
  return GetMap("resource.category")
}

// 监控配置文件变化并热加载程序
func watchConfig(c *viper.Viper, config_name string) {
  c.WatchConfig()
  c.OnConfigChange(func(e fsnotify.Event) {
    log.Println("Config file changed: %s", e.Name)
    if config_name == "config" {
      InitConfig()  
    } else if config_name == "config_extend" {
      InitConfigExtend()  
    }
  })
}

func ConnDB() *gorm.DB {
  DB,err := gorm.Open(mysql.Open(C.Mysql.Dsn),&gorm.Config{})
  if err != nil {
    panic(err)
  }
  //DB.LogMode(true)
  return DB
}